#! /bin/sh
set -e

clean_directories() {
  echo "Cleaning directories..."
  
  find "/home/tezos/.octez-evm-node" -type f -not -name '.gitkeep' -delete
  find "/home/tezos/.octez-evm-node" -type d -empty -delete

  echo "Cleaning completed."
}

wait_for_node() {
  echo "Waiting for service at $ETHERLINK_ENDPOINT to become available..."

  while ! wget --spider --timeout=1 -q "$ETHERLINK_ENDPOINT/global/smart_rollup_address"; do
    echo "Service is not available yet. Retrying..."
    sleep 1
  done

  echo "Service is now available."
}

if [ "$OCTEZ_CLEAN" = "true" ]; then
  clean_directories
fi

# Wait for the node to be ready
wait_for_node

# Run the proxy
octez-evm-node run proxy with endpoint "${ETHERLINK_ENDPOINT}" \
  --rpc-addr 0.0.0.0 \
  --rpc-port 8545
